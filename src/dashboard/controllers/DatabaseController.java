package dashboard.controllers;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.sql.*;

/**
 * @author sqlitetutorial.net
 */
public class DatabaseController {
    /**
     * Connect to a sample database
     */

    private static final String dbName = "HBU_SKEMA.db";

    public static Connection getConnection() {
        return connect();
    }

    public static String getDbName() {
        return dbName;
    }

    private static Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:" + dbName;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    /**
     * Connect to a sample database
     *
     * @param fileName the database file name
     */
    public static void createNewDatabase(String fileName) {

        String url = "jdbc:sqlite:" + fileName;
        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                System.out.println("Connected to database: " + url);
            } else {
                System.out.println("New database created: " + url);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }


    public static void createLektionerTable() {
        // SQLite connection string
        String url = "jdbc:sqlite:" + DatabaseController.getDbName();

        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS Lektioner (\n" +
                "  id          INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  navn        TEXT NOT NULL,\n" +
                "  nummer      INTEGER ," +
                "  beskrivelse TEXT ,\n" +
                "  tid         TEXT ,\n" +
                "  beklaedning TEXT CONSTRAINT Lektioner_Beklaedning_id_fk REFERENCES Beklaedning ON UPDATE CASCADE ON DELETE CASCADE ,\n" +
                "  lokation    TEXT CONSTRAINT Lektioner_Lokation_id_fk REFERENCES Lokationer ON UPDATE CASCADE ON DELETE CASCADE ,\n" +
                "  fag         TEXT CONSTRAINT Lektioner_Fag_id_fk REFERENCES Fag ON UPDATE CASCADE ON DELETE CASCADE ,\n" +
                "  ansvar      TEXT CONSTRAINT Lektioner_Ansvar_id_fk REFERENCES Ansvar ON UPDATE CASCADE ON DELETE CASCADE " +
                ");";
        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void createEnhederTable() {
        // SQLite connection string
        String url = "jdbc:sqlite:" + DatabaseController.getDbName();

        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS Enheder (\n" +
                "  id          INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  navn        TEXT NOT NULL,\n" +
                "  forkortelse TEXT ,\n" +
                "  antal         TEXT" +
                ");";
        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void createBeklæningTable() {
        // SQLite connection string
        String url = "jdbc:sqlite:" + DatabaseController.getDbName();

        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS Beklaedning (\n" +
                "  id          INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  navn        TEXT NOT NULL,\n" +
                "  beskrivelse TEXT NOT NULL\n" +
                ");";
        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void createLokationerTable() {
        // SQLite connection string
        String url = "jdbc:sqlite:" + DatabaseController.dbName;

        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS Lokationer (\n" +
                "  id          INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  navn        TEXT NOT NULL,\n" +
                "  beskrivelse TEXT NOT NULL\n" +
                ");";
        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void createFagTable() {
        // SQLite connection string
        String url = "jdbc:sqlite:" + DatabaseController.dbName;

        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS Fag (\n" +
                "  id          INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  navn        TEXT NOT NULL,\n" +
                "  beskrivelse TEXT NOT NULL,\n" +
                "  timer       TEXT NOT NULL,\n" +
                "  ansvar      TEXT \n" +
                ");";
        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void createAnsvarTable() {
        // SQLite connection string
        String url = "jdbc:sqlite:" + DatabaseController.dbName;

        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS Ansvar (\n" +
                "  id          INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  navn        TEXT NOT NULL,\n" +
                "  forkortelse TEXT NOT NULL UNIQUE \n" +
                ");";
        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void databaseInformation(String fileName) {

        String url = "jdbc:sqlite:" + fileName;

        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("-------------------------------");
                System.out.println(meta.getConnection());
                System.out.println(meta.getDriverName());
                System.out.println(meta.getDriverVersion());
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //connect();
    }


}
