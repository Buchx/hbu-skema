package dashboard.controllers;

import com.jfoenix.controls.*;
import dashboard.models.PDFManagement;
import dashboard.models.PaaklaedningDB;
import dashboard.models.SkemaLektion;
import dashboard.models.SkemaToHtml;
import de.jensd.fx.glyphs.GlyphsBuilder;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.print.*;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.transform.Scale;
import javafx.util.StringConverter;

import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.ResourceBundle;

public class SkemaController implements Initializable {

    @FXML
    AnchorPane skema;
    @FXML
    ScrollPane skemaScroll;
    @FXML
    HBox panel;
    @FXML
    StackPane dialogStack;

    private VBox weekBox = new VBox();
    public static JFXDatePicker dp = new JFXDatePicker();
    public static ArrayList<JFXTimePicker> timePickers = new ArrayList<>();

    private JFXButton ok = new JFXButton("Ja");
    private JFXButton cancel = new JFXButton("Nej");
    private JFXDialog myDialog;
    private JFXDialogLayout layout = new JFXDialogLayout();
    private JFXSnackbar bar;
    private int weekNumber = 0;
    private static String[] weekDays = {"Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "Lørdag", "Søndag"};


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        bar = new JFXSnackbar(dialogStack);
        datePicker();
        //printButton();
        createWeek();
        //saveButton();
        clearButton();
        htmlButton();
        cancel.setOnAction(q -> {
            myDialog.close();
        });


    }

    private void clearButton() {
        JFXButton clear = new JFXButton("Ryd Skema");
        clear.setGraphic(GlyphsBuilder.create(MaterialDesignIconView.class)
                .glyph(MaterialDesignIcon.DELETE)
                .size("20")
                .build());
        panel.getChildren().add(clear);

        clear.setOnAction(e -> {

            layout.setHeading(new Label("Slet alt?"));
            layout.setBody(new Label("Er du sikker på du vil slette alt indhold i tabellen?"));
            ok.setDefaultButton(true);
            layout.setActions(cancel, ok);
            myDialog = new JFXDialog(dialogStack, layout, JFXDialog.DialogTransition.TOP);
            myDialog.show();
            myDialog.requestFocus();

            ok.setOnAction(f -> {
                for (Node day : weekBox.getChildren()) {
                    if (day.getClass().getSimpleName().equalsIgnoreCase("hbox")) {
                        HBox d = (HBox) day;
                        VBox lektioner = (VBox) d.getChildren().get(2);
                        lektioner.getChildren().clear();
                    }
                }
                bar.enqueue(new JFXSnackbar.SnackbarEvent("Alt indhold slettet!"));
                myDialog.close();
            });

        });
    }

    private void htmlButton() {
        JFXButton htmlButton = new JFXButton("Gem som HTML");
        htmlButton.setGraphic(GlyphsBuilder.create(MaterialDesignIconView.class)
                .glyph(MaterialDesignIcon.LANGUAGE_HTML5)
                .size("20")
                .build());
        panel.getChildren().add(htmlButton);

        htmlButton.setOnAction(e -> {
            layout.setHeading(new Label("Oprette HTML"));
            VBox v = new VBox();
            Label message = new Label("Er der mere du vil tilføje?");
            JFXTextArea header = new JFXTextArea("Jydske Livkompagni\n" +
                    "Dragonkasernen 1\n" +
                    "7500 Holstebro");
            header.setLabelFloat(true);
            header.setPromptText("Tekst til toppen af skemaet");
            JFXTextArea footer = new JFXTextArea("Version X");
            footer.setLabelFloat(true);
            footer.setPromptText("Tekst til bunden af skemaet");
            JFXCheckBox pakl = new JFXCheckBox("Vis påklædningsliste?");
            //JFXCheckBox enh = new JFXCheckBox("Vis enhedsliste?");

            v.getChildren().addAll(message, header, footer, pakl);
            v.setStyle("-fx-spacing: 40px;");
            layout.setBody(v);
            ok.setDefaultButton(true);
            layout.setActions(cancel, ok);
            myDialog = new JFXDialog(dialogStack, layout, JFXDialog.DialogTransition.TOP);
            myDialog.show();
            myDialog.requestFocus();

            ok.setOnAction(event -> {
                String extra = "";
                if (pakl.isSelected()) {
                    extra = PaaklaedningDB.visAlleListe();
                }

                saveSkemaAsObject(header.getText(), footer.getText(), extra);
                myDialog.close();
            });

        });
    }


    private void saveSkemaAsObject(String header, String footer, String extras) {
        ArrayList<SkemaLektion> lektionerForugen = new ArrayList<>();

        for (Node day : weekBox.getChildren()) {
            HBox d = (HBox) day;
            VBox daylektures = (VBox) d.getChildren().get(2);
            Label date = (Label) d.getChildren().get(0);

            if(daylektures.getChildren().size() == 0){
                SkemaLektion l = new SkemaLektion();
                l.setWeekDay(date.getText());
                lektionerForugen.add(l);
            }else{
                daylektures.getChildren().forEach(onelek -> {
                    SkemaLektion lekture = new SkemaLektion((HBox) onelek);
                    lekture.setWeekDay(date.getText());
                    lektionerForugen.add(lekture);
                });
            }
        }
        SkemaToHtml.saveWeek("" + weekNumber, header, footer, extras, lektionerForugen);
    }

    private void datePicker() {

        dp.setPromptText("Vælg Mandag hvor ugen starter.");
        dp.showWeekNumbersProperty().setValue(true);
        LocalDate previousMonday =
                LocalDate.now(ZoneId.of("Europe/Paris"))
                        .with(TemporalAdjusters.previous(DayOfWeek.MONDAY));
        dp = new JFXDatePicker(previousMonday);

        WeekFields weekFields = WeekFields.of(Locale.getDefault());
        weekNumber = previousMonday.get(weekFields.weekOfWeekBasedYear());
        Label weekNumLabel = new Label("Uge " + weekNumber);

        panel.getChildren().addAll(dp, weekNumLabel);

        dp.valueProperty().addListener((p, oldValue, newValue) -> {
            if (newValue == null) return;
            WeekFields fields = WeekFields.of(Locale.getDefault());

            // # may range from 0 ... 54 without overlapping the boundaries of calendar year
            //weekNumber = newValue.get(fields.weekOfYear());

            // # may range from 1 ... 53 with overlapping
            weekNumber = newValue.get(fields.weekOfWeekBasedYear());
            weekNumLabel.setText("Uge " + weekNumber);

        });
    }

    private void createWeek() {
        VBox holder = new VBox();

        HBox headerDescription = new HBox(
                new Label(),
                new HBox(),
                new Label("Tid"),
                new Label("Enhed"),
                new Label("Fag"),
                new Label("Lek. Nr."),
                new Label("Aktivitet."),
                new Label("Leder"),
                new Label("Påklædning"),
                new Label("Sted"));
        headerDescription.getStyleClass().add("headerDescription");
        headerDescription.getChildren().get(0).getStyleClass().add("dayLabel");
        headerDescription.getChildren().get(1).getStyleClass().add("titleBox");
        headerDescription.getChildren().get(2).getStyleClass().add("tempLekTid");
        headerDescription.getChildren().get(2).getStyleClass().add("first-correction");
        headerDescription.getChildren().get(3).getStyleClass().add("tempEnhed");
        headerDescription.getChildren().get(4).getStyleClass().add("tempFag");
        headerDescription.getChildren().get(5).getStyleClass().add("tempLekNummer");
        headerDescription.getChildren().get(6).getStyleClass().add("tempName");
        headerDescription.getChildren().get(7).getStyleClass().add("tempAnsvar");
        headerDescription.getChildren().get(8).getStyleClass().add("tempPaaklaedning");
        headerDescription.getChildren().get(9).getStyleClass().add("tempLokation");

        weekBox.setFillWidth(true);

        for (String w : weekDays) {
            VBox dayBox = new VBox();
            dayBox.setId(w);
            JFXButton create = new JFXButton();
            create.setGraphic(GlyphsBuilder.create(MaterialDesignIconView.class)
                    .glyph(MaterialDesignIcon.PLUS_BOX_OUTLINE)
                    .size("20")
                    .build());
            JFXTimePicker tempTid = new JFXTimePicker(LocalTime.of(7, 15));
            tempTid.setIs24HourView(true);
            DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("HH:mm");
            tempTid.converterProperty().setValue(new StringConverter<LocalTime>() {
                @Override
                public String toString(LocalTime object) {
                    return timeFormat.format(object);
                }

                @Override
                public LocalTime fromString(String string) {
                    return LocalTime.parse(string);
                }
            });

            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("eeee\ndd\nMMM");
            Label dayLabel = new Label(dp.getValue().plusDays(Arrays.asList(weekDays).indexOf(w)).format(dateTimeFormatter).toUpperCase());
            HBox titleBox = new HBox(create, tempTid);


            tempTid.getValue().format(timeFormat);
            dayLabel.getStyleClass().add("dayLabel");
            tempTid.getStyleClass().add("tempTid");

            EventHandler<Event> dateHandler = event -> {
                dayLabel.setText(dp.getValue().plusDays(Arrays.asList(weekDays).indexOf(w)).format(dateTimeFormatter).toUpperCase());
            };
            dp.addEventHandler(ActionEvent.ANY, dateHandler);

            HBox day = new HBox(dayLabel, titleBox, dayBox);
            day.getStyleClass().add("day-box");
            dayBox.getStyleClass().add("lektures-box");

            weekBox.getChildren().add(day);

            create.setOnAction(e -> {
                SkemaLektion sk = new SkemaLektion();
                dayBox.getChildren().add(sk.getContent());
            });
        }

        holder.getChildren().addAll(headerDescription, weekBox);
        skemaScroll.setContent(holder);

    }

    /*
    private void readyForPrint() {
        for (Node day : weekBox.getChildren()) {
            if (day.getClass().getSimpleName().equalsIgnoreCase("Hbox")) {
                HBox d = (HBox) day;
                HBox titleBox = (HBox) d.getChildren().get(1);
                VBox lektioner = (VBox) d.getChildren().get(2);

                if (titleBox.getStyleClass().contains("titleBox-print")) {
                    titleBox.getStyleClass().remove("titleBox-print");
                } else {
                    titleBox.getStyleClass().add("titleBox-print");
                }

                lektioner.getChildren().forEach(p -> {
                    HBox oneLek = (HBox) p;
                    if (oneLek.getStyleClass().contains("oneLek-print")) {
                        oneLek.getStyleClass().remove("oneLek-print");
                    } else {
                        oneLek.getStyleClass().add("oneLek-print");
                    }

                    oneLek.getChildren().forEach(a -> {
                        // TODO: Add and remove styles to text fields so they look like a table.
                        if (a.getStyleClass().contains("input-print")) {
                            a.getStyleClass().remove("input-print");
                        } else {
                            a.getStyleClass().add("input-print");
                        }
                        if (a.visibleProperty().get() && a.getClass().getSimpleName().equalsIgnoreCase("JFXButton")) {
                            a.setVisible(false);
                        } else {
                            a.setVisible(true);
                        }
                    });
                });

            }
        }
    }

    private void printButton() {
        JFXButton print = new JFXButton("Print skema");
        print.setGraphic(GlyphsBuilder.create(MaterialDesignIconView.class)
                .glyph(MaterialDesignIcon.PRINTER)
                .size("20")
                .build());
        panel.getChildren().add(print);


        print.setOnAction(e -> {

            layout.setHeading(new Label("Hvor vil du gemme?"));
            JFXTextField inputText = new JFXTextField();

            layout.setBody(
                    new VBox(new Label("Hvad skal titlen være?"),
                            inputText
                    )
            );
            ok.setDefaultButton(true);
            layout.setActions(cancel, ok);
            myDialog = new JFXDialog(dialogStack, layout, JFXDialog.DialogTransition.TOP);

            myDialog.show();

            ok.setOnAction(x -> {
                ArrayList<String> printList = new ArrayList<>();
                for (Node day : weekBox.getChildren()) {
                    if (day.getClass().getSimpleName().equalsIgnoreCase("vbox")) {
                        VBox d = (VBox) day;
                        VBox lektioner = (VBox) d.getChildren().get(2);
                        lektioner.getChildren().forEach(p -> {
                            HBox oneLek = (HBox) p;
                            oneLek.getChildren().forEach(a -> {
                                String singleEntry = "";
                                if (a.getClass().getSimpleName().equalsIgnoreCase("JFXComboBox")) {
                                    JFXComboBox<?> printCombo = (JFXComboBox<?>) a;
                                    if (!printCombo.getSelectionModel().isEmpty()) {
                                        singleEntry += printCombo.getSelectionModel().getSelectedItem().toString();
                                    }
                                }
                                if (a.getClass().getSimpleName().equalsIgnoreCase("JFXTextField")) {
                                    JFXTextField printText = (JFXTextField) a;
                                    singleEntry += printText.getText();
                                }
                                System.out.print(singleEntry);
                                printList.add(singleEntry);
                                //Skema thisWeekSkema = new Skema(LocalDate.now());
                            });
                            System.out.println("");
                        });
                        System.out.println("");
                    }
                }
                PDFManagement.create(printList, inputText.getText());
                myDialog.close();
            });

        });

    }

    private void saveButton() {
        JFXButton btnSave = new JFXButton("Gem som PDF");
        btnSave.setGraphic(GlyphsBuilder.create(MaterialDesignIconView.class)
                .glyph(MaterialDesignIcon.FILE_PDF)
                .size("20")
                .build());
        panel.getChildren().add(btnSave);


        btnSave.setOnAction(e -> {
            //saveSkemaAsObject();

            PrinterJob job = PrinterJob.createPrinterJob();
            Printer printer = job.getPrinter();
            job.getJobSettings().setPrintQuality(PrintQuality.HIGH);
            PageLayout pageLayout = printer.createPageLayout(Paper.A4, PageOrientation.PORTRAIT, Printer.MarginType.HARDWARE_MINIMUM);


            double scaleX = pageLayout.getPrintableWidth() / weekBox.getBoundsInParent().getWidth();
            double scaleY = pageLayout.getPrintableHeight() / weekBox.getBoundsInParent().getHeight();
            Scale scale = new Scale(scaleX, scaleY);
            readyForPrint();
            weekBox.getTransforms().add(scale);
            boolean success = job.printPage(pageLayout, weekBox);


            if (success) {
                job.endJob();
                bar.enqueue(new JFXSnackbar.SnackbarEvent("Filen er gemt!"));
            } else {
                job.endJob();
                bar.enqueue(new JFXSnackbar.SnackbarEvent("Filen blev ikke gemt."));
            }
            weekBox.getTransforms().remove(scale);
            readyForPrint();

});
        }
*/

}


                /*

                EventHandler<Event> handler = event -> {
                    try {
                        // Add get lek nummer count maks.
                        Lektioner l = lekDB.selectOne(
                                tempFag.getSelectionModel().getSelectedItem().toString(),
                                tempLekNummer.getSelectionModel().getSelectedItem().toString());

                        String startTime = "";
                        String endTime = tempTid.getValue().plusMinutes(Long.parseLong(l.getTid().getValue())).toString();

                        // Check if first time of day or not
                        if (dayBox.getChildren().indexOf(tempLekture) == 0) {
                            startTime = tempTid.getValue().format(timeFormat);
                        } else {
                            HBox prevBox = (HBox) dayBox.getChildren().get((dayBox.getChildren().indexOf(tempLekture)) - 1);    // Get previos lekture
                            JFXTextField firstVal = (JFXTextField) prevBox.getChildren().get(1);                                 // Get time filed
                            String[] timeSplit = firstVal.getText().split(" - ");                                         // Split the field and get end time
                            String[] hourMinSplit = timeSplit[1].split(":");                                              // Set start time for previous end time
                            startTime = timeSplit[1];
                            LocalTime timeOflastPrevTime = LocalTime.of(Integer.parseInt(hourMinSplit[0]), Integer.parseInt(hourMinSplit[1]));
                            endTime = timeOflastPrevTime.plusMinutes(Long.parseLong(l.getTid().getValue())).toString();

                            // Check if Enhed is same or not, if same change start and end time, if then ...
                            JFXComboBox<?> prevEnhed = (JFXComboBox<?>) prevBox.getChildren().get(2);
                            if (!prevEnhed.getSelectionModel().isEmpty()) {
                                if (tempEnhed.getSelectionModel().getSelectedItem().equalsIgnoreCase(prevEnhed.getSelectionModel().getSelectedItem().toString())) {
                                    // If they match change nothing
                                } else {
                                    if (dayBox.getChildren().indexOf(tempLekture) == 0) {
                                        // Remember to check if first time enhed chosen to set this time.
                                        startTime = tempTid.getValue().format(timeFormat);
                                    } else {
                                        startTime = tempTid.getValue().format(timeFormat);
                                        String[] hourMinSplit2 = timeSplit[0].split(":");
                                        if (!tempEnhed.getSelectionModel().getSelectedItem().equalsIgnoreCase(prevEnhed.getSelectionModel().getSelectedItem().toString())) {
                                            endTime = tempTid.getValue().plusMinutes(Long.parseLong(l.getTid().getValue())).toString();
                                        } else {
                                            LocalTime prevT = LocalTime.of(Integer.parseInt(hourMinSplit2[0]), Integer.parseInt(hourMinSplit2[1]));
                                            endTime = prevT.plusMinutes(Long.parseLong(l.getTid().getValue())).toString();
                                        }
                                    }
                                }
                            }
                        }
                        */
