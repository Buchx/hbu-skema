package dashboard.controllers;

import com.jfoenix.controls.*;
import com.jfoenix.controls.cells.editors.TextFieldEditorBuilder;
import com.jfoenix.controls.cells.editors.base.GenericEditableTreeTableCell;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.jfoenix.validation.NumberValidator;
import com.jfoenix.validation.RequiredFieldValidator;
import com.jfoenix.validation.ValidationFacade;
import com.jfoenix.validation.base.ValidatorBase;
import dashboard.models.*;
import de.jensd.fx.glyphs.GlyphsBuilder;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;

import java.net.URL;
import java.util.ResourceBundle;

public class LektionerController implements Initializable {


    @FXML
    AnchorPane lektionerAnchor;
    @FXML
    TableView lektionerTable;
    @FXML
    JFXTextField filterField;
    @FXML
    JFXButton addNewBtn;
    @FXML
    StackPane lektionerStack;
    @FXML
    TableColumn<Lektioner, String> fagColumn, nrColumn, navnColumn, beskrivelseColumn, tidColumn, ansvarColumn, lokationColumn, beklaedningColumn;

    // Objects for dialog.
    private GridPane body = new GridPane();
    private JFXTextField inputTid = new JFXTextField();
    private JFXTextField inputName = new JFXTextField();
    private JFXComboBox<Fag> fagCombo = new JFXComboBox<>();
    private JFXTextArea inputBeskrivelse = new JFXTextArea();
    private JFXTextField lekNummer = new JFXTextField();
    private JFXComboBox<Ansvar> ansvarCombo = new JFXComboBox<>();
    private JFXComboBox<Steder> stederCombo = new JFXComboBox<>();
    private JFXComboBox<Paaklaedning> paaklaedningCombo = new JFXComboBox<>();
    private JFXButton ok = new JFXButton("Ok");
    private JFXButton close = new JFXButton("Luk");
    private JFXButton delete = new JFXButton("Slet");
    private JFXButton editOk = new JFXButton("Gem");
    private JFXDialog myDialog;
    private JFXDialogLayout layout;

    private ObservableList<Lektioner> tableData = FXCollections.observableArrayList();
    private static final String EM1 = "1em";
    private static final String ERROR = "error";

    private LektionerDB db = LektionerDB.getDb();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        body.addRow(0, inputName);
        body.addRow(1, fagCombo);
        body.addRow(2, lekNummer);
        body.addRow(3, inputBeskrivelse);
        body.addRow(4, inputTid);
        body.addRow(5, ansvarCombo);
        body.addRow(6, stederCombo);
        body.addRow(7, paaklaedningCombo);

        layout = new JFXDialogLayout();
        myDialog = new JFXDialog(lektionerStack, layout, JFXDialog.DialogTransition.TOP);
        layout.setBody(body);
        ok.setDefaultButton(true);

        body.setVgap(40);
        body.setHgap(20);


        createFilterList();
        initButtons();
    }


    private void initButtons() {
        filterField.requestFocus();

        addNewBtn.setOnAction(e -> {
            addNewBtn.setDisable(true);
            detailDialog(null, false);
        });

    }

    private void createFilterList() {
        lektionerTable.getItems().clear();
        LektionerDB.list.clear();
        LektionerDB.selectAll();

        tableData.addAll(LektionerDB.list);

        // 0. Initialize the columns.
        fagColumn.setCellValueFactory(cellData -> cellData.getValue().getFag());
        nrColumn.setCellValueFactory(cellData -> cellData.getValue().getNummer());
        navnColumn.setCellValueFactory(cellData -> cellData.getValue().getNavn());
        beskrivelseColumn.setCellValueFactory(cellData -> cellData.getValue().getBeskrivelse());
        tidColumn.setCellValueFactory(cellData -> cellData.getValue().getTid());
        ansvarColumn.setCellValueFactory(cellData -> cellData.getValue().getAnsvar());
        lokationColumn.setCellValueFactory(cellData -> cellData.getValue().getLokation());
        beklaedningColumn.setCellValueFactory(cellData -> cellData.getValue().getBeklaedning());

        // 1. Wrap the ObservableList in a FilteredList (initially display all data).
        FilteredList<Lektioner> filteredData = new FilteredList<>(tableData, p -> true);

        // 2. Set the filter Predicate whenever the filter changes.
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(lek -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (lek.getFag().toString().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (lek.getNavn().toString().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                } else if (lek.getBeskrivelse().toString().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches timer
                } else if (lek.getTid().toString().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches ansvar
                } else if (lek.getAnsvar().toString().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches ansvar
                } else if (lek.getLokation().toString().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches ansvar
                } else if (lek.getBeklaedning().toString().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches ansvar
                } else if (lek.getNummer().toString().toLowerCase().contains(lowerCaseFilter)){
                    return true;
                }

                return false; // Does not match.
            });
        });

        // 3. Wrap the FilteredList in a SortedList.
        SortedList<Lektioner> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(lektionerTable.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        lektionerTable.setItems(sortedData);

        EventHandler<Event> handler = event -> {
            Lektioner toEdit = (Lektioner) lektionerTable.getSelectionModel().getSelectedItem();
            detailDialog(toEdit, true);
        };

        lektionerTable.addEventHandler(MouseEvent.MOUSE_CLICKED, handler);
    }

    private void detailDialog(Lektioner lek, boolean edit) {
        initValidation();
        // Construct dialog

        JFXSnackbar bar = new JFXSnackbar(lektionerStack);

        Label heading = new Label();
        if (edit) {
            heading.setText("Rediger " + lek.getNavn().getValue());
        } else {
            heading.setText("Opret ny");
        }
        layout.setHeading(heading);

        Double prefWidth = 400.0;

        // Content for dialog
        inputName.setPromptText("Navn på lektionen eks. \'Skjul og Sløring\'");
        inputName.setLabelFloat(true);
        inputName.setPrefWidth(prefWidth);

        inputBeskrivelse.setPromptText("Beskrivelse af lektionen. eks. KVK");
        inputBeskrivelse.setLabelFloat(true);
        inputBeskrivelse.setPrefWidth(prefWidth);

        inputTid.setPromptText("Tid i minutter til lektionen. Eks. \'90\' (1.5 time) ");
        inputTid.setLabelFloat(true);
        inputTid.setPrefWidth(prefWidth);

        ansvarCombo.setPromptText("Hvem er ansvarlig for lektionen?");
        ansvarCombo.setLabelFloat(true);
        ansvarCombo.setPrefWidth(prefWidth);

        stederCombo.setPromptText("Hvor bør lektionen afholdes?");
        stederCombo.setLabelFloat(true);
        stederCombo.setPrefWidth(prefWidth);

        fagCombo.setPromptText("Hvilket fag hører lektionen under?");
        fagCombo.setLabelFloat(true);
        fagCombo.setPrefWidth(prefWidth);

        paaklaedningCombo.setPromptText("Hvad skal eleverne have på under din lektion?");
        paaklaedningCombo.setLabelFloat(true);
        paaklaedningCombo.setPrefWidth(prefWidth);

        lekNummer.setPromptText("Lektions nummer");
        lekNummer.setLabelFloat(true);
        lekNummer.setPrefWidth(prefWidth);


        //populate from DB.
        AnsvarDB.list.clear();
        AnsvarDB.selectAll();
        ansvarCombo.getItems().clear();
        for (Ansvar a : AnsvarDB.list) {
            ansvarCombo.getItems().add(a);
        }

        StederDB.list.clear();
        StederDB stederDB = StederDB.getSteder();
        stederCombo.getItems().clear();
        stederDB.selectAll();
        for (Steder s : StederDB.list) {
            stederCombo.getItems().add(s);
        }

        FagDB.list.clear();
        FagDB fagDB = FagDB.getDb();
        fagCombo.getItems().clear();
        fagDB.selectAll();
        for (Fag f : FagDB.list) {
            fagCombo.getItems().add(f);
        }

        PaaklaedningDB.list.clear();
        PaaklaedningDB.selectAll();
        paaklaedningCombo.getItems().clear();
        for (Paaklaedning p : PaaklaedningDB.list) {
            paaklaedningCombo.getItems().add(p);
        }


        // Set cell factorys (What to show in comboboxes)
        ansvarCombo.setCellFactory(param -> new ListCell<>() {

            @Override
            protected void updateItem(Ansvar item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null || item.getForkortelse() == null) {
                    setText(null);
                } else {
                    setText(item.toString());
                }
            }
        });

        stederCombo.setCellFactory(param -> new ListCell<>() {
            @Override
            protected void updateItem(Steder i, boolean empty) {
                super.updateItem(i, empty);
                if (empty || i == null || i.getNavn() == null) {
                    setText(null);
                } else {
                    setText(i.toString());
                }
            }
        });

        fagCombo.setCellFactory(param -> new ListCell<>() {
            @Override
            protected void updateItem(Fag i, boolean empty) {
                super.updateItem(i, empty);
                if (empty || i == null || i.getNavn() == null) {
                    setText(null);
                } else {
                    setText(i.toString());
                }
            }
        });

        paaklaedningCombo.setCellFactory(param -> new ListCell<>() {
            @Override
            protected void updateItem(Paaklaedning i, boolean empty) {
                super.updateItem(i, empty);
                if (empty || i == null || i.getNavn() == null) {
                    setText(null);
                } else {
                    setText(i.toString());
                }
            }
        });


        // If edit chosen, populate fields with data.
        body.setPrefWidth(prefWidth);
        layout.setPrefWidth(prefWidth);
        if (edit) {
            layout.setActions(delete, close, editOk);
            editOk.setDefaultButton(true);
            inputName.requestFocus();
            inputName.setText(lek.getNavn().getValue());
            inputBeskrivelse.setText(lek.getBeskrivelse().getValue());
            lekNummer.setText(lek.getNummer().getValue());
            inputTid.setText(lek.getTid().getValue());
            for (Ansvar a : AnsvarDB.list) {
                if (a.toString().equalsIgnoreCase(lek.getAnsvar().getValue())) {
                    ansvarCombo.getSelectionModel().select(a);
                }
            }
            for (Fag f : FagDB.list) {
                if (f.toString().equalsIgnoreCase(lek.getFag().getValue())) {
                    fagCombo.getSelectionModel().select(f);
                }
            }
            for (Paaklaedning a : PaaklaedningDB.list) {
                if (a.toString().equalsIgnoreCase(lek.getBeklaedning().getValue())) {
                    paaklaedningCombo.getSelectionModel().select(a);
                }
            }
            for (Steder a : StederDB.list) {
                if (a.toString().equalsIgnoreCase(lek.getLokation().getValue())) {
                    stederCombo.getSelectionModel().select(a);
                }
            }

        } else {
            inputName.requestFocus();
            ok.setDefaultButton(true);
            layout.setActions(close, ok);
        }


        myDialog.setOverlayClose(false);
        myDialog.show();
        myDialog.requestFocus();
        inputName.requestFocus();


        // BUTTON EVENTS
        close.setOnAction(e -> {
            myDialog.close();
        });

        ok.setOnAction(e -> {
            // If all fields validate return true, then insert into DB.
            if (inputName.validate()
                    & lekNummer.validate()
                    & inputBeskrivelse.validate()
                    & inputTid.validate()
                    & !ansvarCombo.getSelectionModel().isEmpty()
                    & !paaklaedningCombo.getSelectionModel().isEmpty()
                    & !fagCombo.getSelectionModel().isEmpty()
                    & !stederCombo.getSelectionModel().isEmpty()) {
                ok.setDisable(true);

                db.insert(inputName.getText(),
                        Integer.parseInt(lekNummer.getText()),
                        inputBeskrivelse.getText(),
                        inputTid.getText(),
                        paaklaedningCombo.getSelectionModel().getSelectedItem().toString(),
                        stederCombo.getSelectionModel().getSelectedItem().toString(),
                        fagCombo.getSelectionModel().getSelectedItem().toString(),
                        ansvarCombo.getSelectionModel().getSelectedItem().toString());

                bar.enqueue(new JFXSnackbar.SnackbarEvent("Lektion oprettet."));
                myDialog.close();
            } else {
                bar.enqueue(new JFXSnackbar.SnackbarEvent("Husk at udfylde alle felter!"));
            }
        });

        editOk.setOnAction(event -> {
            if (inputName.validate()
                    & lekNummer.validate()
                    & inputBeskrivelse.validate()
                    & inputTid.validate()
                    & !ansvarCombo.getSelectionModel().isEmpty()
                    & !paaklaedningCombo.getSelectionModel().isEmpty()
                    & !fagCombo.getSelectionModel().isEmpty()
                    & !stederCombo.getSelectionModel().isEmpty()) {

                // perform updates
                db.update(lek.getId(), "navn", inputName.getText());
                db.update(lek.getId(), "nummer", lekNummer.getText());
                db.update(lek.getId(), "beskrivelse", inputBeskrivelse.getText());
                db.update(lek.getId(), "tid", inputTid.getText());
                db.update(lek.getId(), "beklaedning", paaklaedningCombo.getSelectionModel().getSelectedItem().toString());
                db.update(lek.getId(), "lokation", stederCombo.getSelectionModel().getSelectedItem().toString());
                db.update(lek.getId(), "fag", fagCombo.getSelectionModel().getSelectedItem().toString());
                db.update(lek.getId(), "ansvar", ansvarCombo.getSelectionModel().getSelectedItem().toString());

                editOk.setDisable(true);
                bar.enqueue(new JFXSnackbar.SnackbarEvent(lek.getNavn().getValue() + " blev opdateret!"));
                myDialog.close();
            } else {
                bar.enqueue(new JFXSnackbar.SnackbarEvent("Husk at udfylde alle felter!"));
            }
        });

        delete.setOnAction(e -> {
            JFXDialogLayout deleteLayout = new JFXDialogLayout();
            JFXDialog deleteDialog = new JFXDialog(lektionerStack, deleteLayout, JFXDialog.DialogTransition.TOP);
            JFXButton confirm = new JFXButton("Ja");
            JFXButton regret = new JFXButton("Nej");
            deleteLayout.setHeading(new Label("Er du sikker?"));
            deleteLayout.setBody(new Label("Er du sikker på du vil slette: " + lek.getNavn().getValue()));
            deleteLayout.setActions(regret, confirm);
            regret.setDefaultButton(true);
            deleteDialog.show();

            confirm.setOnAction(co -> {
                delete.setDisable(true);
                bar.enqueue(new JFXSnackbar.SnackbarEvent("Lektion slettet!"));
                db.delete(lek.getId());
                myDialog.close();
                deleteDialog.close();
            });

            regret.setOnAction(re-> deleteDialog.close());
        });

        myDialog.setOnDialogClosed(e -> {
            ok.setDisable(false);
            editOk.setDisable(false);
            delete.setDisable(false);
            addNewBtn.setDisable(false);

            inputName.clear();
            inputBeskrivelse.clear();
            inputTid.clear();
            lekNummer.clear();
            ansvarCombo.getSelectionModel().clearSelection();
            fagCombo.getSelectionModel().clearSelection();
            paaklaedningCombo.getSelectionModel().clearSelection();
            stederCombo.getSelectionModel().clearSelection();


            lektionerTable.getItems().removeAll();
            tableData.clear();
            LektionerDB.list.clear();
            LektionerDB.selectAll();
            tableData.addAll(LektionerDB.list);
        });


    }

    private void initValidation() {
        //INPUT VALIDATION
        RequiredFieldValidator nameVal = new RequiredFieldValidator();
        nameVal.setMessage("Input krævet.");
        nameVal.setIcon(GlyphsBuilder.create(FontAwesomeIconView.class)
                .glyph(FontAwesomeIcon.INFO_CIRCLE)
                .size(EM1)
                .styleClass(ERROR)
                .build());
        inputName.getValidators().add(nameVal);
        inputName.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                inputName.validate();
            }
        });


        NumberValidator lekVal = new NumberValidator();
        lekVal.setMessage("Skal være heltal eks. 8");
        lekVal.setIcon(GlyphsBuilder.create(FontAwesomeIconView.class)
                .glyph(FontAwesomeIcon.INFO_CIRCLE)
                .size(EM1)
                .styleClass(ERROR)
                .build());
        lekNummer.getValidators().add(lekVal);
        lekNummer.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                lekNummer.validate();
            }
        });

        RequiredFieldValidator beskVal = new RequiredFieldValidator();
        beskVal.setMessage("Input krævet.");
        beskVal.setIcon(GlyphsBuilder.create(FontAwesomeIconView.class)
                .glyph(FontAwesomeIcon.INFO_CIRCLE)
                .size(EM1)
                .styleClass(ERROR)
                .build());
        inputBeskrivelse.getValidators().add(beskVal);
        inputBeskrivelse.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                inputBeskrivelse.validate();
            }
        });

        NumberValidator tidVal = new NumberValidator();
        tidVal.setMessage("Skal være heltal eks. 8");
        tidVal.setIcon(GlyphsBuilder.create(FontAwesomeIconView.class)
                .glyph(FontAwesomeIcon.INFO_CIRCLE)
                .size(EM1)
                .styleClass(ERROR)
                .build());
        inputTid.getValidators().add(tidVal);
        inputTid.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                inputTid.validate();
            }
        });


    }


}

