package dashboard.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class HomeController implements Initializable {


    @FXML
    AnchorPane home;
    @FXML
    ImageView bg1;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        bg1.fitWidthProperty().bind(home.widthProperty());
        bg1.fitHeightProperty().bind(home.heightProperty());
        bg1.setPreserveRatio(false);
        bg1.setSmooth(true);
        bg1.setOpacity(0.15);
    }


    public static String generateRandomChars(String candidateChars, int length) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(candidateChars.charAt(random.nextInt(candidateChars
                    .length())));
        }

        return sb.toString();
    }
}
