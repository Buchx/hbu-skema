package dashboard.controllers;

import com.jfoenix.controls.*;
import com.jfoenix.validation.RequiredFieldValidator;
import dashboard.models.Ansvar;
import dashboard.models.AnsvarDB;
import dashboard.models.Enheder;
import dashboard.models.EnhederDB;
import de.jensd.fx.glyphs.GlyphsBuilder;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;

public class EnhederController implements Initializable {

    @FXML
    EnhederDB enhederDB = EnhederDB.getDb();

    ObservableList<Enheder> tableData = FXCollections.observableArrayList();
    @FXML
    AnchorPane enheder;

    @FXML
    JFXTextField filterField;
    @FXML
    JFXButton addNewBtn;
    @FXML
    TableView ansvarTable;
    @FXML
    TableColumn<Enheder, String> forkortelseColumn;
    @FXML
    TableColumn<Enheder, String> navnColumn;
    @FXML
    StackPane enhederStack;
    JFXDialog myDialog;
    JFXDialogLayout layout;


    private static final String EM1 = "1em";
    private static final String ERROR = "error";


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        createFilterList();
        newBtn();

    }

    private void newBtn() {
        addNewBtn.setOnAction(e -> {
            detailDialog(null, false);
        });
    }

    private void createFilterList() {
        tableData.clear();
        ansvarTable.getItems().clear();
        EnhederDB.list.clear();

        EnhederDB.selectAll();
        tableData.addAll(EnhederDB.list);
        filterField.requestFocus();

        // 0. Initialize the columns.
        forkortelseColumn.setCellValueFactory(cellData -> cellData.getValue().getForkortelse());
        navnColumn.setCellValueFactory(cellData -> cellData.getValue().getNavn());

        // 1. Wrap the ObservableList in a FilteredList (initially display all data).
        FilteredList<Enheder> filteredData = new FilteredList<>(tableData, p -> true);

        // 2. Set the filter Predicate whenever the filter changes.
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(enheder -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (enheder.getNavn().toString().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (enheder.getForkortelse().toString().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                return false; // Does not match.
            });
        });

        // 3. Wrap the FilteredList in a SortedList.
        SortedList<Enheder> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(ansvarTable.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        ansvarTable.setItems(sortedData);

        EventHandler<Event> handler = event -> {
            Enheder toEdit = (Enheder) ansvarTable.getSelectionModel().getSelectedItem();
            detailDialog(toEdit, true);
        };

        ansvarTable.addEventHandler(MouseEvent.MOUSE_CLICKED, handler);
    }

    private void detailDialog(Enheder enheder, boolean edit) {


        /*
        CONNSTRUCTION
         */
        layout = new JFXDialogLayout();
        myDialog = new JFXDialog(enhederStack, layout, JFXDialog.DialogTransition.TOP);
        JFXSnackbar bar = new JFXSnackbar(enhederStack);


        Label heading = new Label();
        if (edit) {
            heading.setText("Rediger: " + enheder.getNavn().getValue());
        } else {
            heading.setText("Opret ny");
        }
        layout.setHeading(heading);
        JFXTextField inputName = new JFXTextField();
        JFXTextField inputForkortelse = new JFXTextField();
        GridPane body = new GridPane();
        JFXButton ok = new JFXButton("Ok");
        JFXButton close = new JFXButton("Luk");
        JFXButton delete = new JFXButton("Slet");
        JFXButton editOk = new JFXButton("Gem");

        body.setVgap(30);
        body.setHgap(20);
        body.setStyle("-fx-padding: 20px;");

        // Content for body
        inputName.setPromptText("Enheds navn eks. 1. Deling");
        inputName.setLabelFloat(true);

        inputForkortelse.setPromptText("Enhed forkortelse eks. 1DEL eller KMP");
        inputForkortelse.setLabelFloat(true);

        body.addRow(0, inputName);
        body.addRow(1, inputForkortelse);

        layout.setBody(body);
        if (edit) {
            layout.setActions(delete, close, editOk);
            editOk.setDefaultButton(true);
            inputName.requestFocus();
            inputName.setText(enheder.getNavn().getValue());
            inputForkortelse.setText(enheder.getForkortelse().getValue());
        } else {
            inputName.requestFocus();
            ok.setDefaultButton(true);
            layout.setActions(close, ok);
        }


        myDialog.setOverlayClose(true);
        myDialog.show();
        myDialog.requestFocus();
        inputName.requestFocus();

        /*
        BUTTON EVENTS
         */
        close.setOnAction(e -> {
            myDialog.close();
        });

        ok.setOnAction(e -> {
            enhederDB.insert(inputName.getText(),
                    inputForkortelse.getText());
            bar.enqueue(new JFXSnackbar.SnackbarEvent("Leder oprettet!"));
            myDialog.close();
        });

        editOk.setOnAction(event -> {
            enhederDB.update(enheder.getId(), "navn", inputName.getText());
            enhederDB.update(enheder.getId(), "forkortelse", inputForkortelse.getText());
            bar.enqueue(new JFXSnackbar.SnackbarEvent(enheder.getNavn().getValue() + " blev opdateret!"));
            myDialog.close();
        });

        delete.setOnAction(e -> {
            JFXDialogLayout deleteLayout = new JFXDialogLayout();
            JFXDialog deleteDialog = new JFXDialog(enhederStack, deleteLayout, JFXDialog.DialogTransition.TOP);
            JFXButton confirm = new JFXButton("Ja");
            JFXButton regret = new JFXButton("Nej");
            deleteLayout.setHeading(new Label("Er du sikker?"));
            deleteLayout.setBody(new Label("Er du sikker på du vil slette: " + enheder.getNavn().getValue()));
            deleteLayout.setActions(regret, confirm);
            regret.setDefaultButton(true);
            deleteDialog.show();

            confirm.setOnAction(co -> {
                delete.setDisable(true);
                bar.enqueue(new JFXSnackbar.SnackbarEvent("Enhed slettet!"));
                enhederDB.delete(enheder.getId());
                myDialog.close();
                deleteDialog.close();
            });

            regret.setOnAction(re-> deleteDialog.close());

        });

        myDialog.setOnDialogClosed(e -> {
            inputName.clear();
            inputForkortelse.clear();

            ansvarTable.getItems().removeAll();
            tableData.clear();
            EnhederDB.list.clear();
            enhederDB.selectAll();
            tableData.addAll(EnhederDB.list);
        });

        /*
        INPUT VALIDATION
         */
        RequiredFieldValidator validator = new RequiredFieldValidator();
        validator.setMessage("Input krævet.");
        validator.setIcon(GlyphsBuilder.create(FontAwesomeIconView.class)
                .glyph(FontAwesomeIcon.WARNING)
                .size(EM1)
                .styleClass(ERROR)
                .build());
        inputForkortelse.getValidators().add(validator);
        inputForkortelse.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                inputForkortelse.validate();
            }
        });

        inputName.getValidators().add(validator);
        inputName.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                inputName.validate();
            }
        });
    }

}
