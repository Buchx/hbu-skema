package dashboard.controllers;

import com.jfoenix.controls.*;
import com.jfoenix.validation.RequiredFieldValidator;
import dashboard.models.*;
import de.jensd.fx.glyphs.GlyphsBuilder;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.FadeTransition;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import javafx.util.StringConverter;

import java.net.URL;
import java.util.ResourceBundle;

public class FagController implements Initializable {


    @FXML
    AnchorPane fag;
    @FXML
    JFXTextField filterField;
    @FXML
    JFXButton addNewBtn, showPie;
    @FXML
    TableView ansvarTable;
    @FXML
    TableColumn<Fag, String> navnColumn, beskrivelseColumn, timerColumn, ansvarColumn;
    @FXML
    StackPane fagStack;

    private JFXDialog myDialog;
    private JFXDialogLayout layout;
    private FagDB db = FagDB.getDb();
    private ObservableList<Fag> tableData = FXCollections.observableArrayList();
    private static final String EM1 = "1em";
    private static final String ERROR = "error";
    private PieChart myChart = new PieChart();
    private ObservableList<PieChart.Data> fagData = FXCollections.observableArrayList();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        createFilterList();
        newBtn();

    }


    private void pieChart() {
        // Obs farverne gentager sig, hvordan får jeg det til at vise unikke farver til hvert fag.
        myChart.getData().clear();
        fagData.clear();
        myChart.setLegendVisible(true);
        myChart.setLabelsVisible(false);
        myChart.setTitle("Antal timer i fagene.");
        myChart.setLegendSide(Side.RIGHT);



        for (Fag f : FagDB.list) {
            PieChart.Data d = new PieChart.Data(f.getNavn().getValue(), Integer.parseInt(f.getTimer().getValue()));
            if (!myChart.getData().contains(d)) {
                fagData.add(d);
            }
        }
        myChart.getData().setAll(fagData);
        myChart.autosize();
        fagData.forEach(data -> data.nameProperty().bind(Bindings.concat(data.getName(), " - ", data.pieValueProperty(), " T")));

    }


    private void newBtn() {
        addNewBtn.setOnAction(e -> {
            detailDialog(null, false);
        });
        showPie.setOnAction(e -> {
            pieChart();
            myDialog = new JFXDialog(fagStack, myChart, JFXDialog.DialogTransition.TOP);
            myDialog.show();
        });
    }

    private void createFilterList() {
        tableData.clear();
        ansvarTable.getItems().clear();
        FagDB.list.clear();

        db.selectAll();
        tableData.addAll(FagDB.list);
        filterField.requestFocus();

        // 0. Initialize the columns.
        navnColumn.setCellValueFactory(cellData -> cellData.getValue().getNavn());
        beskrivelseColumn.setCellValueFactory(cellData -> cellData.getValue().getBeskrivelse());
        timerColumn.setCellValueFactory(cellData -> cellData.getValue().getTimer());
        ansvarColumn.setCellValueFactory(cellData -> cellData.getValue().getAnsvar());

        // 1. Wrap the ObservableList in a FilteredList (initially display all data).
        FilteredList<Fag> filteredData = new FilteredList<>(tableData, p -> true);

        // 2. Set the filter Predicate whenever the filter changes.
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(fag -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (fag.getNavn().toString().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (fag.getBeskrivelse().toString().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                } else if (fag.getTimer().toString().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches timer
                } else if (fag.getAnsvar().toString().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches ansvar
                }
                return false; // Does not match.
            });
        });

        // 3. Wrap the FilteredList in a SortedList.
        SortedList<Fag> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(ansvarTable.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        ansvarTable.setItems(sortedData);

        EventHandler<Event> handler = event -> {
            Fag toEdit = (Fag) ansvarTable.getSelectionModel().getSelectedItem();
            detailDialog(toEdit, true);
        };

        ansvarTable.addEventHandler(MouseEvent.MOUSE_CLICKED, handler);
    }


    private void detailDialog(Fag fag, boolean edit) {
        // Construct dialog
        layout = new JFXDialogLayout();
        myDialog = new JFXDialog(fagStack, layout, JFXDialog.DialogTransition.TOP);
        JFXSnackbar bar = new JFXSnackbar(fagStack);


        Label heading = new Label();
        if (edit) {
            heading.setText("Rediger " + fag.getNavn().getValue());
        } else {
            heading.setText("Opret ny");
        }
        layout.setHeading(heading);
        JFXComboBox<Ansvar> jfxCombo = new JFXComboBox<>();
        JFXTextField inputName = new JFXTextField();
        JFXTextField inputBeskrivelse = new JFXTextField();
        JFXTextField inputTimer = new JFXTextField();
        GridPane body = new GridPane();
        JFXButton ok = new JFXButton("Ok");
        JFXButton close = new JFXButton("Luk");
        JFXButton delete = new JFXButton("Slet");
        JFXButton editOk = new JFXButton("Gem");

        body.setVgap(30);
        body.setHgap(20);
        body.setStyle("-fx-padding: 20px;");

        // Content for dialog
        inputName.setPromptText("Forkortelse på faget. Eks. FØHJ");
        inputName.setLabelFloat(true);

        inputBeskrivelse.setPromptText("Fagets fulde navn. Eks. Førstehjælp");
        inputBeskrivelse.setLabelFloat(true);

        inputTimer.setPromptText("Antal timer til faget");
        inputTimer.setLabelFloat(true);


        //populate from DB.
        AnsvarDB.list.clear();
        AnsvarDB.selectAll();
        for (Ansvar a : AnsvarDB.list) {
            jfxCombo.getItems().add(a);
        }

        jfxCombo.setCellFactory(param -> new ListCell<Ansvar>() {
            @Override
            protected void updateItem(Ansvar item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null || item.getForkortelse() == null) {
                    setText(null);
                } else {
                    setText("(" + item.getForkortelse().getValue() + ") " + item.getNavn().getValue());
                }
            }
        });

        jfxCombo.setConverter(new StringConverter<Ansvar>() {
            @Override
            public String toString(Ansvar item) {
                if (item == null) {
                    return null;
                } else {
                    return "(" + item.getForkortelse().getValue() + ") " + item.getNavn().getValue();
                }
            }

            @Override
            public Ansvar fromString(String userId) {
                return null;
            }
        });
        jfxCombo.setPromptText("Hvem er faglærer/ansvar?");
        jfxCombo.setLabelFloat(true);

        body.addRow(0, inputName);
        body.addRow(1, inputBeskrivelse);
        body.addRow(2, inputTimer);
        body.addRow(3, jfxCombo);

        // If edit chosen, populate fields with data.
        layout.setBody(body);
        if (edit) {
            layout.setActions(delete, close, editOk);
            editOk.setDefaultButton(true);
            inputName.requestFocus();
            inputName.setText(fag.getNavn().getValue());
            inputBeskrivelse.setText(fag.getBeskrivelse().getValue());
            inputTimer.setText(fag.getTimer().getValue());
            for (Ansvar a : AnsvarDB.list) {
                if (a.toString().equalsIgnoreCase(fag.getAnsvar().getValue())) {
                    jfxCombo.getSelectionModel().select(a);
                }
            }
        } else {
            inputName.requestFocus();
            ok.setDefaultButton(true);
            layout.setActions(close, ok);
        }


        myDialog.setOverlayClose(true);
        myDialog.show();
        myDialog.requestFocus();
        inputName.requestFocus();


        // BUTTON EVENTS

        close.setOnAction(e -> {
            myDialog.close();
        });

        ok.setOnAction(e -> {
            db.insert(inputName.getText(),
                    inputBeskrivelse.getText(),
                    inputTimer.getText(),
                    jfxCombo.getSelectionModel().getSelectedItem().toString());
            bar.enqueue(new JFXSnackbar.SnackbarEvent("Fag oprettet."));
            myDialog.close();
        });

        editOk.setOnAction(event -> {
            db.update(fag.getId(), "navn", inputName.getText());
            db.update(fag.getId(), "beskrivelse", inputBeskrivelse.getText());
            db.update(fag.getId(), "timer", inputTimer.getText());
            db.update(fag.getId(), "ansvar", jfxCombo.getSelectionModel().getSelectedItem().toString());
            bar.enqueue(new JFXSnackbar.SnackbarEvent(fag.getNavn().getValue() + " blev opdateret!"));
            myDialog.close();
        });

        delete.setOnAction(e -> {
            JFXDialogLayout deleteLayout = new JFXDialogLayout();
            JFXDialog deleteDialog = new JFXDialog(fagStack, deleteLayout, JFXDialog.DialogTransition.TOP);
            JFXButton confirm = new JFXButton("Ja");
            JFXButton regret = new JFXButton("Nej");
            deleteLayout.setHeading(new Label("Er du sikker?"));
            deleteLayout.setBody(new Label("Er du sikker på du vil slette: " + fag.getNavn().getValue()));
            deleteLayout.setActions(regret, confirm);
            regret.setDefaultButton(true);
            deleteDialog.show();

            confirm.setOnAction(co -> {
                delete.setDisable(true);
                bar.enqueue(new JFXSnackbar.SnackbarEvent("Lektion slettet!"));
                db.delete(fag.getId());
                myDialog.close();
                deleteDialog.close();
            });

            regret.setOnAction(re-> deleteDialog.close());

        });

        myDialog.setOnDialogClosed(e -> {
            inputName.clear();
            inputBeskrivelse.clear();
            inputTimer.clear();
            jfxCombo.getSelectionModel().clearSelection();

            ansvarTable.getItems().removeAll();
            tableData.clear();
            FagDB.list.clear();
            db.selectAll();
            tableData.addAll(FagDB.list);
        });

        /*
        INPUT VALIDATION
         */
        RequiredFieldValidator validator = new RequiredFieldValidator();
        validator.setMessage("Input krævet.");
        validator.setIcon(GlyphsBuilder.create(FontAwesomeIconView.class)
                .glyph(FontAwesomeIcon.WARNING)
                .size(EM1)
                .styleClass(ERROR)
                .build());
        inputBeskrivelse.getValidators().add(validator);
        inputBeskrivelse.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                inputBeskrivelse.validate();
            }
        });

        inputName.getValidators().add(validator);
        inputName.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                inputName.validate();
            }
        });

        inputTimer.getValidators().add(validator);
        inputTimer.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                inputTimer.validate();
            }
        });
    }

}
