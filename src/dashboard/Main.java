package dashboard;

import dashboard.controllers.DatabaseController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));

        Image windowIcon = new Image("file:forsvaret_logo_icon.png");
        primaryStage.getIcons().add(windowIcon);

        primaryStage.setTitle("HBU SKEMA");
        primaryStage.setScene(new Scene(root));
        primaryStage.setMaximized(true);
        primaryStage.show();

        //Initialize DB before UI in separate thread.
        initDatabase();
    }

    private void initDatabase(){

        // Start thread for seperate loading
        new Thread(() -> {
            DatabaseController.createNewDatabase("HBU_SKEMA.db"); //If not made create database "First time creation"
            DatabaseController.createLektionerTable();      // Lektioner
            DatabaseController.createAnsvarTable();         // Ansvar
            DatabaseController.createBeklæningTable();      // Beklædning
            DatabaseController.createFagTable();            // Fag
            DatabaseController.createLokationerTable();     // Lokationer
            DatabaseController.createEnhederTable();        //
        }).start();
    }


    public static void main(String[] args) {
        launch(args);
    }
}

