package dashboard.models;

import com.jfoenix.controls.JFXButton;
import javafx.beans.property.StringProperty;

public class Steder {
    private int id;
    private StringProperty navn;
    private StringProperty beskrivelse;

    public Steder(int id, StringProperty navn, StringProperty beskrivelse) {
        this.id = id;
        this.navn = navn;
        this.beskrivelse = beskrivelse;
    }


    public int getId() {
        return id;
    }

    public StringProperty getNavn() {
        return navn;
    }

    public StringProperty getBeskrivelse() {
        return beskrivelse;
    }

    @Override
    public String toString(){
        return this.navn.getValue();
    }
}
