package dashboard.models;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;



import java.io.FileNotFoundException;
import java.util.ArrayList;



public class PDFManagement {
    public static final String FONT = "./assets/Fonts/Roboto-Medium.ttf";
    public static final String FONTBOLD = "./assets/Fonts/Roboto-Bold.ttf";
    private static final String DEST = "simple_table.pdf";



    public static void create(ArrayList<String> al, String titleText) {
        try {

            PdfDocument pdfDoc = null;
            pdfDoc = new PdfDocument(new PdfWriter(DEST));
            pdfDoc.setDefaultPageSize(PageSize.A4);


            Document doc = new Document(pdfDoc);

            doc.add(new Paragraph(titleText));
            doc.setFontSize(10);

            Table table = new Table(8);
            table.addHeaderCell("Tid");
            table.addHeaderCell("Enhed");
            table.addHeaderCell("Fag");
            table.addHeaderCell("LekNr");
            table.addHeaderCell("Emne");
            table.addHeaderCell("Ansvar");
            table.addHeaderCell("Påklædning");
            table.addHeaderCell("Sted");
            table.useAllAvailableWidth();

            for (String s : al) {
                if (!s.isEmpty()) {
                    Cell c = new Cell();
                    c.add(new Paragraph(s));
                    //c.setBorderTop(new SolidBorder(Color.makeColor(DeviceRgb.GREEN.getColorSpace()), 1));
                    table.addCell(c);
                }
            }
            doc.add(table);

            Table mon = new Table(8);
            mon.addHeaderCell("MANDAG");
            mon.addHeaderCell("Enhed");
            mon.addHeaderCell("Fag");
            mon.addHeaderCell("LekNr");
            mon.addHeaderCell("Emne");
            mon.addHeaderCell("Ansvar");
            mon.addHeaderCell("Påklædning");
            mon.addHeaderCell("Sted");
            mon.useAllAvailableWidth();

            for (String s : al) {
                if (!s.isEmpty()) {
                    Cell c = new Cell();
                    c.add(new Paragraph(s));
                    //c.setBorderTop(new SolidBorder(Color.makeColor(DeviceRgb.GREEN.getColorSpace()), 1));
                    table.addCell(c);
                }
            }
            doc.add(table);

            doc.close();

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (NoClassDefFoundError q) {
            System.out.println(q.getCause().toString());
        }
    }


}
