package dashboard.models;

import javafx.beans.property.StringProperty;

public class Paaklaedning {
    private int id;
    private StringProperty navn;
    private StringProperty beskrivelse;

    public Paaklaedning(int id, StringProperty navn, StringProperty beskrivelse) {
        this.id = id;
        this.navn = navn;
        this.beskrivelse = beskrivelse;
    }


    public int getId() {
        return id;
    }

    public StringProperty getNavn() {
        return navn;
    }

    public StringProperty getBeskrivelse() {
        return beskrivelse;
    }

    @Override
    public String toString() {
        return this.navn.getValue();
    }
}

