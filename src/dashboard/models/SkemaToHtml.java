package dashboard.models;

import dashboard.controllers.SkemaController;
import htmlflow.HtmlView;
import htmlflow.elements.HtmlBody;
import htmlflow.elements.HtmlTable;
import htmlflow.elements.HtmlTr;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.util.ArrayList;


public class SkemaToHtml {

    public static void saveHtml() {
        HtmlView<?> taskView = new HtmlView<>();
        taskView
                .head()
                .title("Uge 37")
                .linkCss("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css");
        taskView
                .body().classAttr("container")
                .heading(1, "Uge 37")
                .hr()
                .div()
                .text("Title: ").text("ISEL MPD project")
                .br()
                .text("Description: ").text("A Java library for serializing objects in HTML.")
                .br()
                .text("Priority: ").text("HIGH")
                .div()
                .table().tr().th().a("asd").text("Hello");

        //
        // Produces an HTML file document
        //

        try (PrintStream out = new PrintStream(new FileOutputStream("Task.html"))) {
            taskView.setPrintStream(out).write();
            Desktop.getDesktop().browse(URI.create("Task.html"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveWeek(
            String weekNum,
            String header,
            String footer,
            String extrasFooter,
            ArrayList<SkemaLektion> lektures) {

        footer = footer.replaceAll("[\n]", "<br />");
        header = header.replaceAll("[\n]", "<br />");
        extrasFooter = extrasFooter.replaceAll("[\n]", "<br />");

        HtmlView<Iterable<SkemaLektion>> taskView = new HtmlView<Iterable<SkemaLektion>>();


        taskView
                .head()
                .title(weekNum)
                //.linkCss("https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.0/paper.min.css")
                .linkCss("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css")
                .scriptBlock()
                .code("</script>\n<meta charset='UTF-8'>\n<script>");

        HtmlBody<Iterable<SkemaLektion>> body = taskView
                .body()
                .classAttr("container");

        // Set header text
        body
                .div()
                .text(header);


        HtmlTable<Iterable<SkemaLektion>> table = body
                .heading(2, "Øvelsesliste for uge " + weekNum.replaceAll("[-]", " "))
                .table().classAttr("table");

        // Set footer text
        body
                .br()
                .div()
                .text(footer);

        if (!extrasFooter.isEmpty()) {
            body
                    .br()
                    .div()
                    .text(extrasFooter);
        }

        String[] last = {""};
        HtmlTr<Iterable<SkemaLektion>> headerRow = table.tr();
        headerRow.th().text("Uge " + weekNum.replaceAll("[-]", " ") + " - " + SkemaController.dp.getValue().getYear());
        headerRow.th().text("Tid");
        headerRow.th().text("Enhed");
        headerRow.th().text("Fag");
        headerRow.th().text("Lek Nr");
        headerRow.th().text("Aktivitet");
        headerRow.th().text("Leder");
        headerRow.th().text("Påklædning");
        headerRow.th().text("Sted");
        table.trFromIterable(
                skemaLektion -> {
                    if (last[0].equals(skemaLektion.getWeekDay())) return "&nbsp;";
                    last[0] = skemaLektion.getWeekDay();
                    return "<div id='" + skemaLektion.getWeekDay().replaceAll("[\n]]","").substring(0, skemaLektion.getWeekDay().length() - 8) + "' style='white-space: pre-wrap;'>" + skemaLektion.getWeekDay() + "</div>";
                },
                skemaLektion -> skemaLektion.getLektid().getText(),
                skemaLektion -> skemaLektion.getEnhedValue(),
                skemaLektion -> skemaLektion.getFagValue(),
                skemaLektion -> skemaLektion.getLekNummerValue(),
                skemaLektion -> skemaLektion.getAktivitet().getText(),
                skemaLektion -> skemaLektion.getAnsvar().getText(),
                skemaLektion -> skemaLektion.getPaaklaedning().getText(),
                (SkemaLektion skemaLektion2) -> skemaLektion2.getLokation().getText());


        String fileName = "Uge_" + weekNum + "_" + SkemaController.dp.getValue().getYear() + ".html";
        try {
            PrintStream out = new PrintStream(new FileOutputStream(fileName), true, "UTF-8");
            taskView.setPrintStream(out).write(lektures);

            Desktop.getDesktop().browse(URI.create(fileName));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
