package dashboard.models;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import dashboard.controllers.SkemaController;
import de.jensd.fx.glyphs.GlyphsBuilder;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.time.LocalTime;
import java.util.ArrayList;

public class SkemaLektion {
    private JFXButton delete;
    private JFXTextField lektid;
    private JFXComboBox<String> enhed;
    private JFXComboBox<Fag> fag;
    private JFXComboBox<Integer> lekNummer;
    private JFXTextField aktivitet;
    private JFXTextField ansvar;
    private JFXTextField lokation;
    private JFXTextField paaklaedning;
    private HBox content;
    private LocalTime startTid = LocalTime.of(7, 15);
    private LocalTime slutTid = null;
    private VBox parent;
    private LektionerDB lekDB = LektionerDB.getDb();
    private JFXTimePicker lektureTimePicker;
    private String weekDay = "";

    public SkemaLektion() {
        this.delete = new JFXButton();
        this.lektid = new JFXTextField();
        this.fag = new JFXComboBox<>();
        this.enhed = new JFXComboBox<>();
        this.lekNummer = new JFXComboBox<>();
        this.aktivitet = new JFXTextField();
        this.ansvar = new JFXTextField();
        this.lokation = new JFXTextField();
        this.paaklaedning = new JFXTextField();
        // Apply all fields to shown HBOX

        Platform.runLater(() -> {
            if (!enhed.isFocused()) {
                enhed.requestFocus();
            }
        });

        deletebutton();
        styling();
        defaultStates();
        fillData();
        events();
        this.content = new HBox(delete, lektid, enhed, fag, lekNummer, aktivitet, ansvar, lokation, paaklaedning);
    }

    public SkemaLektion(HBox input) {

        this.delete = (JFXButton) input.getChildren().get(0);
        this.lektid = (JFXTextField) input.getChildren().get(1);
        this.enhed = (JFXComboBox<String>) input.getChildren().get(2);
        this.fag = (JFXComboBox<Fag>) input.getChildren().get(3);
        this.lekNummer = (JFXComboBox<Integer>) input.getChildren().get(4);
        this.aktivitet = (JFXTextField) input.getChildren().get(5);
        this.ansvar = (JFXTextField) input.getChildren().get(6);
        this.lokation = (JFXTextField) input.getChildren().get(7);
        this.paaklaedning = (JFXTextField) input.getChildren().get(8);
        this.content = input;
        Platform.runLater(() -> {
            if (!enhed.isFocused()) {
                enhed.requestFocus();
            }
        });

        deletebutton();
        styling();
        fillData();
        events();
    }



    private void deletebutton() {
        delete.setGraphic(GlyphsBuilder.create(MaterialDesignIconView.class)
                .glyph(MaterialDesignIcon.DELETE)
                .size("18")
                .build());
        this.delete.setOnAction(e -> {
            parent.getChildren().remove(content);
        });
    }


    private void events() {

        EventHandler<Event> enableNext = event -> {
            if (enhed.isFocused() & !enhed.getSelectionModel().isEmpty()) {
                fag.setDisable(false);
                fag.requestFocus();
            } else if (fag.isFocused() & !fag.getSelectionModel().isEmpty()) {
                lekNummer.setDisable(false);
                lekNummer.requestFocus();
                lekNummer.getItems().clear();
                ArrayList<Integer> lekNumre = lekDB.getLekNumre(fag.getSelectionModel().getSelectedItem().toString());
                if (!lekNumre.isEmpty()) {
                    lekNummer.setDisable(false);
                    for (Integer i : lekNumre) {
                        lekNummer.getItems().add(i);
                    }
                } else {
                    lekNummer.setDisable(true);
                    fag.requestFocus();
                }
            }
        };


        EventHandler<Event> handler = event -> {
            try {

                Lektioner l = lekDB.selectOne(
                        fag.getSelectionModel().getSelectedItem().toString(),
                        lekNummer.getSelectionModel().getSelectedItem().toString());

                int indexNumber = parent.getChildren().indexOf(content);
                int prevIndex;

                if (indexNumber <= 0) {
                    prevIndex = indexNumber;
                } else {
                    prevIndex = indexNumber - 1;
                }

                HBox prevBox = (HBox) parent.getChildren().get(prevIndex);
                JFXTextField prevTimeBox = (JFXTextField) prevBox.getChildren().get(1);
                JFXComboBox<?> prevEnhed = (JFXComboBox<?>) prevBox.getChildren().get(2);
                String[] prevTimes = prevTimeBox.getText().split(" - "); // 0 will be first time 1 will be end time.

                //If first node only set end time.
                if (indexNumber == 0) {
                    slutTid = startTid.plusMinutes(Long.parseLong(l.getTid().getValue()));
                } else {
                    startTid = LocalTime.parse(prevTimes[1]);
                    slutTid = startTid.plusMinutes(Long.parseLong(l.getTid().getValue()));
                }

                // If selected enhed matches porevios enhed, do nothing if not change starttime to original starttime.
                if (!prevEnhed.getSelectionModel().isEmpty()) {
                    if (!prevEnhed.getSelectionModel().getSelectedItem().equals(enhed.getSelectionModel().getSelectedItem())) {
                        startTid = lektureTimePicker.getValue();
                        slutTid = startTid.plusMinutes(Long.parseLong(l.getTid().getValue()));
                    } else {
                        if (indexNumber == 0) {
                            startTid = lektureTimePicker.getValue();
                            slutTid = startTid.plusMinutes(Long.parseLong(l.getTid().getValue()));
                        } else {
                            if (enhed.getSelectionModel().getSelectedItem().equalsIgnoreCase(prevEnhed.getSelectionModel().getSelectedItem().toString())) {
                                startTid = LocalTime.parse(prevTimes[1]);
                                slutTid = startTid.plusMinutes(Long.parseLong(l.getTid().getValue()));
                            } else {
                                startTid = lektureTimePicker.getValue();
                                slutTid = LocalTime.parse(prevTimes[0]);
                            }
                        }
                    }
                }


                lektid.setDisable(false);
                aktivitet.setDisable(false);
                lokation.setDisable(false);
                ansvar.setDisable(false);
                paaklaedning.setDisable(false);

                lektid.setText(startTid.toString() + " - " + slutTid.toString());
                aktivitet.setText(l.getNavn().getValue());
                lokation.setText(l.getLokation().getValue());
                ansvar.setText(l.getAnsvar().getValue().replaceAll("[a-zA-ZæøåÆØÅ.();:_<> ]", ""));
                paaklaedning.setText(l.getBeklaedning().getValue());

            } catch (NullPointerException v) {
                lektid.setText("");
                aktivitet.setText("");
                lokation.setText("");
                ansvar.setText("");
                paaklaedning.setText("");

                lektid.setDisable(true);
                aktivitet.setDisable(true);
                lokation.setDisable(true);
                ansvar.setDisable(true);
                paaklaedning.setDisable(true);
            }
        };

        fag.addEventHandler(ActionEvent.ANY, handler);
        lekNummer.addEventHandler(ActionEvent.ANY, handler);
        enhed.addEventHandler(ActionEvent.ANY, handler);
        lektid.addEventHandler(ActionEvent.ANY, handler);
        delete.addEventHandler(ActionEvent.ANY, handler);


        Platform.runLater(() -> {
            parent = (VBox) content.getParent();
            HBox parentTwo = (HBox) parent.getParent();
            HBox datepickerHolder = (HBox) parentTwo.getChildren().get(1);
            lektureTimePicker = (JFXTimePicker) datepickerHolder.getChildren().get(1);
            lektureTimePicker.addEventHandler(ActionEvent.ANY, handler);
        });

        enhed.addEventHandler(ActionEvent.ANY, enableNext);
        fag.addEventHandler(ActionEvent.ANY, enableNext);
        lekNummer.addEventHandler(ActionEvent.ANY, enableNext);

    }

    private void styling() {
        Platform.runLater(() -> {
            this.content.setStyle("-fx-spacing: 5px;");
            this.delete.getStyleClass().add("");
            this.lektid.getStyleClass().add("tempLekTid");
            this.fag.getStyleClass().add("tempFag");
            this.enhed.getStyleClass().add("tempEned");
            this.lekNummer.getStyleClass().add("tempLekNummer");
            this.aktivitet.getStyleClass().add("tempName");
            this.ansvar.getStyleClass().add("tempAnsvar");
            this.lokation.getStyleClass().add("tempLokation");
            this.paaklaedning.getStyleClass().add("tempPaaklaedning");
        });
    }

    private void fillData() {
        // Fill in dropdowns
        FagDB fagDB = FagDB.getDb();
        FagDB.list.clear();
        fagDB.selectAll();
        for (Fag f : FagDB.list) {
            fag.getItems().add(f);
        }

        EnhederDB.list.clear();
        EnhederDB.selectAll();
        for (Enheder e : EnhederDB.list) {
            enhed.getItems().add(e.getForkortelse().getValue());
        }


    }

    private void defaultStates() {
        //Editable to false
        aktivitet.setEditable(false);
        lektid.setEditable(false);
        ansvar.setEditable(false);
        lokation.setEditable(false);
        paaklaedning.setEditable(false);

        aktivitet.setDisable(true);
        lektid.setDisable(true);
        ansvar.setDisable(true);
        lokation.setDisable(true);
        paaklaedning.setDisable(true);
        lekNummer.setDisable(true);
        fag.setDisable(true);

        //Able to "tab" through disabled.
        delete.setFocusTraversable(false);
    }

    public HBox getContent() {
        return content;
    }

    public JFXTextField getLektid() {
        return lektid;
    }

    public JFXComboBox<String> getEnhed() {
        return enhed;
    }
    public String getEnhedValue() {
        if(enhed.getSelectionModel().isEmpty()){
            return " ";
        }else{
            return enhed.getSelectionModel().getSelectedItem();
        }
    }

    public JFXComboBox<Fag> getFag() {
        return fag;
    }
    public String getFagValue() {
        if(fag.getSelectionModel().isEmpty()){
            return " ";
        }else{
            return fag.getSelectionModel().getSelectedItem().toString();
        }
    }

    public JFXComboBox<Integer> getLekNummer() {
        return lekNummer;
    }

    public String getLekNummerValue() {
        if(lekNummer.getSelectionModel().isEmpty()){
            return " ";
        }else{
            return lekNummer.getSelectionModel().getSelectedItem().toString();
        }
    }

    public JFXTextField getAktivitet() {
        return aktivitet;
    }

    public JFXTextField getAnsvar() {
        return ansvar;
    }

    public JFXTextField getLokation() {
        return lokation;
    }

    public JFXTextField getPaaklaedning() {
        return paaklaedning;
    }

    public LocalTime getStartTid() {
        return startTid;
    }

    public LocalTime getSlutTid() {
        return slutTid;
    }

    public void setWeekDay(String weekDay) {
        this.weekDay = weekDay;
    }

    public String getWeekDay() {
        return weekDay;
    }

    @Override
    public String toString() {
        return "SkemaLektion{" +
                "lektid=" + lektid.getText() +
                ", enhed=" + enhed.getSelectionModel().getSelectedItem() +
                ", fag=" + fag.getSelectionModel().getSelectedItem().getNavn().getValue() +
                ", lekNummer=" + lekNummer.getSelectionModel().getSelectedItem().toString() +
                ", aktivitet=" + aktivitet.getText() +
                ", ansvar=" + ansvar.getText() +
                ", lokation=" + lokation.getText() +
                ", paaklaedning=" + paaklaedning.getText() +
                '}';
    }
}
