package dashboard.models;

import dashboard.controllers.DatabaseController;
import javafx.beans.property.SimpleStringProperty;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;

public class LektionerDB {

    private static LektionerDB db = new LektionerDB();
    public static String table = "Lektioner";
    public static ArrayList<Lektioner> list = new ArrayList<>();

    private LektionerDB(){
        // Singleton object
    }

    public static LektionerDB getDb(){
        return db;
    }


    private Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:" + DatabaseController.getDbName();
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }


    public void insert(String navn, int nummer, String beskrivelse, String tid, String beklaedning, String lokation, String fag, String ansvar) {
        String sql = "INSERT INTO " + table + "(navn, nummer, beskrivelse,tid,beklaedning,lokation,fag,ansvar) VALUES(?,?,?,?,?,?,?,?)";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, navn);
            pstmt.setInt(2, nummer);
            pstmt.setString(3, beskrivelse);
            pstmt.setString(4, tid);
            pstmt.setString(5, beklaedning);
            pstmt.setString(6, lokation);
            pstmt.setString(7, fag);
            pstmt.setString(8, ansvar);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void update(int id, String column, String content) {
        String sql = "UPDATE " + table + " SET " + column + " = ? WHERE id = ?";

        try (Connection conn = DatabaseController.getConnection();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            // set the corresponding param
            pstmt.setString(1, content);
            pstmt.setInt(2, id);
            // update
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void delete(int id) {
        String sql = "DELETE FROM " + table + " WHERE id = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setInt(1, id);
            // execute the delete statement
            pstmt.executeUpdate();
            pstmt.closeOnCompletion();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void view(int id) {
        String sql = "SELECT * FROM " + table + " WHERE id = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
            pstmt.closeOnCompletion();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public int getMaksLektioner() {
        String sql = "SELECT nummer FROM " + table + " WHERE nummer = (SELECT max(nummer) FROM Lektioner)";
        int max = 0;
        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()){
                max = rs.getInt("nummer");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return max;
    }

    public ArrayList<Integer> getLekNumre(String fag) {
        String sql = "SELECT nummer FROM " + table + " WHERE fag = ?";
        ArrayList<Integer> numbers = new ArrayList<>();

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1 ,fag);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()){
                numbers.add(rs.getInt("nummer"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        // Creating LinkedHashSet
        LinkedHashSet<Integer> lhs = new LinkedHashSet<Integer>();

        /* Adding ArrayList elements to the LinkedHashSet
         * in order to remove the duplicate elements and
         * to preserve the insertion order.
         */
        lhs.addAll(numbers);

        // Removing ArrayList elements
        numbers.clear();

        // Adding LinkedHashSet elements to the ArrayList
        numbers.addAll(lhs);
        Collections.sort(numbers);
        return numbers;
    }

    public Lektioner selectOne(String fag, String lekNummer){
        String sql = "SELECT * FROM " + table + " WHERE nummer=? AND fag=?";
        Lektioner lek = null;

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, lekNummer);
            pstmt.setString(2, fag);
            ResultSet rs = pstmt.executeQuery();


            while (rs.next()) {
                lek = new Lektioner(
                                rs.getInt("id"),
                                new SimpleStringProperty(rs.getString("navn")),
                                new SimpleStringProperty(rs.getString("nummer")),
                                new SimpleStringProperty(rs.getString("beskrivelse")),
                                new SimpleStringProperty(rs.getString("tid")),
                                new SimpleStringProperty(rs.getString("beklaedning")),
                                new SimpleStringProperty(rs.getString("lokation")),
                                new SimpleStringProperty(rs.getString("fag")),
                                new SimpleStringProperty(rs.getString("ansvar"))
                );
            }
            rs.close();
            return lek;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return lek;
    }

    public static void selectAll() {
        String sql = "SELECT * FROM " + table;
        try (Connection conn = DatabaseController.getConnection();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            // loop through the result set
            while (rs.next()) {
                list.add(new Lektioner(
                        rs.getInt("id"),
                        new SimpleStringProperty(rs.getString("navn")),
                        new SimpleStringProperty(rs.getString("nummer")),
                        new SimpleStringProperty(rs.getString("beskrivelse")),
                        new SimpleStringProperty(rs.getString("tid")),
                        new SimpleStringProperty(rs.getString("beklaedning")),
                        new SimpleStringProperty(rs.getString("lokation")),
                        new SimpleStringProperty(rs.getString("fag")),
                        new SimpleStringProperty(rs.getString("ansvar"))
                        )
                );
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}