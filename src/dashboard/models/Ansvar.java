package dashboard.models;

import javafx.beans.property.StringProperty;

public class Ansvar {
    private int id;
    private StringProperty navn;
    private StringProperty forkortelse;

    public Ansvar(int id, StringProperty navn, StringProperty forkortelse) {
        this.id = id;
        this.navn = navn;
        this.forkortelse = forkortelse;
    }


    public int getId() {
        return id;
    }

    public StringProperty getNavn() {
        return navn;
    }

    public StringProperty getForkortelse() {
        return forkortelse;
    }

    @Override
    public String toString(){
        return ("(" + this.forkortelse.getValue() + ") " + this.navn.getValue());
    }

}
