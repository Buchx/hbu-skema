package dashboard.models;


import javafx.beans.property.StringProperty;

public class Fag {

    private int id;
    private StringProperty navn;
    private StringProperty beskrivelse;
    private StringProperty timer;
    private StringProperty ansvar;

    public Fag(int id, StringProperty navn, StringProperty beskrivelse, StringProperty timer, StringProperty ansvar) {
        this.id = id;
        this.navn = navn;
        this.beskrivelse = beskrivelse;
        this.timer = timer;
        this.ansvar = ansvar;
    }

    public int getId() {
        return id;
    }

    public StringProperty getNavn() {
        return navn;
    }

    public StringProperty getBeskrivelse() {
        return beskrivelse;
    }

    public StringProperty getTimer() {
        return timer;
    }

    public StringProperty getAnsvar() {
        return ansvar;
    }

    @Override
    public String toString(){
        return (""+this.navn.getValue()+"");
    }
}
