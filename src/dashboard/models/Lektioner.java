package dashboard.models;

import javafx.beans.property.StringProperty;

public class Lektioner {
    private int id;
    private StringProperty navn;
    private StringProperty nummer;
    private StringProperty beskrivelse;
    private StringProperty tid;
    private StringProperty beklaedning;
    private StringProperty lokation;
    private StringProperty fag;
    private StringProperty ansvar;

    public Lektioner(int id, StringProperty navn, StringProperty nummer, StringProperty beskrivelse, StringProperty tid, StringProperty beklaedning, StringProperty lokation, StringProperty fag, StringProperty ansvar) {
        this.id = id;
        this.navn = navn;
        this.nummer = nummer;
        this.beskrivelse = beskrivelse;
        this.tid = tid;
        this.beklaedning = beklaedning;
        this.lokation = lokation;
        this.fag = fag;
        this.ansvar = ansvar;
    }

    public int getId() {
        return id;
    }


    public StringProperty getNavn() {
        return navn;
    }

    public StringProperty getNummer() {
        return nummer;
    }

    public StringProperty getBeskrivelse() {
        return beskrivelse;
    }

    public StringProperty getTid() {
        return tid;
    }

    public StringProperty getBeklaedning() {
        return beklaedning;
    }

    public StringProperty getLokation() {
        return lokation;
    }

    public StringProperty getFag() {
        return fag;
    }

    public StringProperty getAnsvar() {
        return ansvar;
    }
}
