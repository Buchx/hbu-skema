package dashboard.models;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

public class Skema {

    LocalDate week;
    String headerText;
    String footerText;

    LocalDate[] dates;

    ArrayList<?> mandag = new ArrayList<>();
    ArrayList<?> tirsdag = new ArrayList<>();
    ArrayList<?> onsdag = new ArrayList<>();
    ArrayList<?> torsdag = new ArrayList<>();
    ArrayList<?> fredag = new ArrayList<>();
    ArrayList<?> lordag = new ArrayList<>();
    ArrayList<?> sondag = new ArrayList<>();

    public Skema(LocalDate week, String headerText, String footerText, LocalDate[] dates, ArrayList<?> mandag, ArrayList<?> tirsdag, ArrayList<?> onsdag, ArrayList<?> torsdag, ArrayList<?> fredag, ArrayList<?> lordag, ArrayList<?> sondag) {
        this.week = week;
        this.headerText = headerText;
        this.footerText = footerText;
        this.dates = dates;
        this.mandag = mandag;
        this.tirsdag = tirsdag;
        this.onsdag = onsdag;
        this.torsdag = torsdag;
        this.fredag = fredag;
        this.lordag = lordag;
        this.sondag = sondag;
    }

    public LocalDate getWeek() {
        return week;
    }

    public String getHeaderText() {
        return headerText;
    }

    public String getFooterText() {
        return footerText;
    }

    public LocalDate[] getDates() {
        return dates;
    }

    public ArrayList<?> getMandag() {
        return mandag;
    }

    public ArrayList<?> getTirsdag() {
        return tirsdag;
    }

    public ArrayList<?> getOnsdag() {
        return onsdag;
    }

    public ArrayList<?> getTorsdag() {
        return torsdag;
    }

    public ArrayList<?> getFredag() {
        return fredag;
    }

    public ArrayList<?> getLordag() {
        return lordag;
    }

    public ArrayList<?> getSondag() {
        return sondag;
    }

    @Override
    public String toString() {
        return "Skema{" +
                "week=" + week +
                ", headerText='" + headerText + '\'' +
                ", footerText='" + footerText + '\'' +
                ", dates=" + Arrays.toString(dates) +
                ", mandag=" + mandag +
                ", tirsdag=" + tirsdag +
                ", onsdag=" + onsdag +
                ", torsdag=" + torsdag +
                ", fredag=" + fredag +
                ", lordag=" + lordag +
                ", sondag=" + sondag +
                '}';
    }
}
