package dashboard.models;

import dashboard.controllers.DatabaseController;
import dashboard.models.Fag;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.sql.*;
import java.util.ArrayList;

public class PaaklaedningDB {

    public static String table = "Beklaedning";
    public static ArrayList<Paaklaedning> list = new ArrayList<>();

    private static PaaklaedningDB db = new PaaklaedningDB();

    private PaaklaedningDB(){
        // Singleton object
    }

    public static PaaklaedningDB getDb(){
        return db;
    }

    public void insert(String navn, String beskrivelse) {
        String sql = "INSERT INTO " + table + "(navn,beskrivelse) VALUES(?,?)";

        try (Connection conn = DatabaseController.getConnection();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, navn);
            pstmt.setString(2, beskrivelse);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void update(int id, String column, String content) {
        String sql = "UPDATE " + table + " SET " + column + " = ? WHERE id = ?";

        try (Connection conn = DatabaseController.getConnection();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            // set the corresponding param
            pstmt.setString(1, content);
            pstmt.setInt(2, id);
            // update
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void delete(int id) {
        String sql = "DELETE FROM " + table + " WHERE id = ?";

        try (Connection conn = DatabaseController.getConnection();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setInt(1, id);
            // execute the delete statement
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void view(int id) {
        String sql = "SELECT * FROM " + table + " WHERE id = ?";

        try (Connection conn = DatabaseController.getConnection();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void selectAll() {
        String sql = "SELECT * FROM " + table;
        try (Connection conn = DatabaseController.getConnection();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            // loop through the result set
            while (rs.next()) {
                list.add(new Paaklaedning(
                        rs.getInt("id"),
                        new SimpleStringProperty(rs.getString("navn")),
                        new SimpleStringProperty(rs.getString("beskrivelse")))
                );
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static String visAlleListe() {
        String sql = "SELECT * FROM " + table;
        String output = "";
        try (Connection conn = DatabaseController.getConnection();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            // loop through the result set
            while (rs.next()) {
                output+= "<b>"+rs.getString("navn") + "</b>\t - " + rs.getString("beskrivelse") + "\n";
            }
            return output;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return output;
    }


}

