package dashboard.models;

import dashboard.controllers.DatabaseController;
import dashboard.models.Fag;
import javafx.beans.property.SimpleStringProperty;

import java.sql.*;
import java.util.ArrayList;

public class FagDB {

    private String table = "Fag";
    public static ArrayList<Fag> list = new ArrayList<>();

    private static FagDB db = new FagDB();

    private FagDB() {

    }

    public static FagDB getDb() {
        return db;
    }

    private Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:" + DatabaseController.getDbName();
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    public void insert(String navn, String beskrivelse, String timer, String ansvar) {
        String sql = "INSERT INTO " + table + "(navn,beskrivelse,timer,ansvar) VALUES(?,?,?,?)";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, navn);
            pstmt.setString(2, beskrivelse);
            pstmt.setString(3, timer);
            pstmt.setString(4, ansvar);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void update(int id, String column, String content) {
        String sql = "UPDATE " + table + " SET " + column + " = ? WHERE id = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, content);
            pstmt.setInt(2, id);
            // update
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void delete(int id) {
        String sql = "DELETE FROM " + table + " WHERE id = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setInt(1, id);
            // execute the delete statement
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void view(int id) {
        String sql = "SELECT * FROM " + table + " WHERE id = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void selectAll() {
        String sql = "SELECT * FROM " + table;
        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            // loop through the result set
            while (rs.next()) {
                list.add(new Fag(
                        rs.getInt("id"),
                        new SimpleStringProperty(rs.getString("navn")),
                        new SimpleStringProperty(rs.getString("beskrivelse")),
                        new SimpleStringProperty(rs.getString("timer")),
                        new SimpleStringProperty(rs.getString("ansvar"))
                ));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }


}

