/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dashboard;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.transitions.hamburger.HamburgerBasicCloseTransition;
import javafx.animation.*;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FXMLDocumentController implements Initializable {

    @FXML
    AnchorPane myAnchor;
    @FXML
    private AnchorPane holderPane;
    @FXML
    public VBox btnGroup;
    @FXML
    public JFXHamburger btnDrawer;
    @FXML
    public JFXDrawer leftDrawer;
    @FXML
    public StackPane drawerStack, dialogStack;


    public static AnchorPane home, settings, skema, lektioner, lokationer, fag, ansvar, paaklaedning, enheder;
    private static ProgressIndicator waitIndicator = new ProgressIndicator();
    private static JFXSpinner spinner = new JFXSpinner();
    private HamburgerBasicCloseTransition burgerTask;
    private TranslateTransition navAnimation;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        listenHolderPane();
        createDrawer();
        //new Thread(this::createDrawer).start();


        Platform.runLater(() -> {
            //Load all fxmls in a cache
            try {
                fag = FXMLLoader.load(getClass().getResource("views/Fag.fxml"));
                home = FXMLLoader.load(getClass().getResource("views/Home.fxml"));
                skema = FXMLLoader.load(getClass().getResource("views/Skema.fxml"));
                ansvar = FXMLLoader.load(getClass().getResource("views/Ansvar.fxml"));
                enheder = FXMLLoader.load(getClass().getResource("views/Enheder.fxml"));
                settings = FXMLLoader.load(getClass().getResource("views/Settings.fxml"));
                lektioner = FXMLLoader.load(getClass().getResource("views/Lektioner.fxml"));
                lokationer = FXMLLoader.load(getClass().getResource("views/Steder.fxml"));
                paaklaedning = FXMLLoader.load(getClass().getResource("views/Paaklaedning.fxml"));
                //Set first node to be shown
                setNode(lektioner);
            } catch (IOException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NullPointerException e) {
                System.out.println(e.getMessage());
            }
        });
    }

    private void listenHolderPane() {
        // Width
        holderPane.widthProperty().addListener((ChangeListener) (observable, oldValue, newValue) -> {
            for (Node n : holderPane.getChildren()) {
                AnchorPane m = (AnchorPane) n;
                m.setPrefWidth((double) newValue);
                showHideMenu((Double) newValue);
            }
        });
        // Height
        holderPane.heightProperty().addListener((ChangeListener) (observable, oldValue, newValue) -> {
            for (Node n : holderPane.getChildren()) {
                AnchorPane m = (AnchorPane) n;
                m.setPrefHeight((double) newValue);
            }
        });

    }

    private void showHideMenu(double value) {
        Double xsmall = 600.0;
        Double small = 960.0;
        Double medium = 1280.0;
        Double large = 1440.0;
        navAnimation = new TranslateTransition(new Duration(350), holderPane);

        if (value >= large || value >= medium) { // Large or Medium
            leftDrawer.open();
            holderPane.getChildren().forEach(f -> {
                holderPane.setRightAnchor(f, 200.0);
            });
            navAnimation.setToX(200);
            burgerTask.setRate(1);
        } else if (value >= small || value >= xsmall) { // Small or Xsmall
            leftDrawer.close();
            holderPane.getChildren().forEach(f -> {
                holderPane.setRightAnchor(f, 0.0);
            });
            burgerTask.setRate(-1);
            navAnimation.setToX(0);
        }
        burgerTask.play();
        navAnimation.play();
    }

    private void createDrawer() {

        burgerTask = new HamburgerBasicCloseTransition(btnDrawer);
        //Initialize and show drawer
        leftDrawer.setResizableOnDrag(true);
        leftDrawer.setSidePane(drawerStack);
        leftDrawer.setDefaultDrawerSize(200);
        leftDrawer.setOnDrawerClosing(e -> {
            holderPane.getChildren().forEach(f -> {
                holderPane.setRightAnchor(f, 0.0);
            });
        });
        leftDrawer.setOnDrawerOpening(e -> {
            holderPane.getChildren().forEach(f -> {
                holderPane.setRightAnchor(f, 200.0);
            });
        });

        burgerTask.setRate(-1);

        //holderPane.setTranslateX(200);
        //Animation open and close
        navAnimation = new TranslateTransition(new Duration(350), holderPane);
        btnDrawer.addEventHandler(MouseEvent.MOUSE_PRESSED, (e) -> {
            burgerTask.setRate(burgerTask.getRate() * -1);
            burgerTask.play();
            if (leftDrawer.isShown()) {
                navAnimation.setToX(0);
                leftDrawer.close();
            } else {
                navAnimation.setToX(200);
                leftDrawer.open();
            }
            navAnimation.play();
        });
        navAnimation.setToX(200);
        leftDrawer.open();
        burgerTask.playFromStart();
        navAnimation.play();
    }

    private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().removeAll();

        // Create spinner
        spinner.setPrefSize(200, 200);
        spinner.setLayoutX((holderPane.getScene().getWidth() - spinner.getPrefWidth()) / 2);
        spinner.setLayoutY((holderPane.getScene().getHeight() - spinner.getPrefHeight()) / 2);
        spinner.setRadius(50);
        AnchorPane master = (AnchorPane) holderPane.getParent();
        master.getChildren().addAll(spinner);

        // Fade transition
        FadeTransition ft = new FadeTransition(Duration.millis(850), node);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);

        // Make timeline animation
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.ZERO,
                        new KeyValue(waitIndicator.progressProperty(), 0),
                        new KeyValue(waitIndicator.progressProperty(), 0)),
                new KeyFrame(Duration.millis(300),
                        new KeyValue(waitIndicator.progressProperty(), 1),
                        new KeyValue(waitIndicator.progressProperty(), 1)));


        timeline.setOnFinished(e -> {
            master.getChildren().remove(spinner);
            //holderPane.getChildren().remove(spinner);
            holderPane.getChildren().add(node);
            AnchorPane temp = (AnchorPane) holderPane.getChildren().get(0);
            if (leftDrawer.isShown()) {
                holderPane.getChildren().forEach(f -> {
                    temp.setRightAnchor(f, 200.0);
                    temp.setTopAnchor(f, 0.0);
                    temp.setLeftAnchor(f, 0.0);
                    temp.setBottomAnchor(f, 0.0);
                    //holderPane.setRightAnchor(f, 200.0);
                });
            } else {
                holderPane.getChildren().forEach(f -> {
                    temp.setRightAnchor(f, 0.0);
                    temp.setTopAnchor(f, 0.0);
                    temp.setLeftAnchor(f, 0.0);
                    temp.setBottomAnchor(f, 0.0);
                    //holderPane.setRightAnchor(f, 0.0);
                });
            }
        });

        ParallelTransition pt = new ParallelTransition(node, ft, timeline);
        pt.play();


    }

    @FXML
    public void switchFag(ActionEvent event) {
        setNode(fag);
    }

    @FXML
    public void switchHome(ActionEvent event) {
        setNode(home);
    }

    @FXML
    public void switchSkema(ActionEvent event) {
        setNode(skema);
    }

    @FXML
    public void switchAnsvar(ActionEvent event) {
        setNode(ansvar);
    }

    @FXML
    public void switchEnheder(ActionEvent event) {
        setNode(enheder);
    }

    @FXML
    public void switchSettings(ActionEvent event) {
        setNode(settings);
    }

    @FXML
    public void switchSteder(ActionEvent event) {
        setNode(lokationer);
    }

    @FXML
    public void switchLektioner(ActionEvent event) {
        setNode(lektioner);
    }

    @FXML
    public void switchPaaklaedning(ActionEvent event) {
        setNode(paaklaedning);
    }


}
